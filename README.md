# pages_test
Settings->Project Name->51aba.gitlab.io

- создаем coverage html 
- кладем его в htmlcov/subdir/
- в .gitlab-ci.yml копируем htmlcov/ в public/
- там же в enviroments -> url прописываем url для доступа к странице из вебинтерфейса gitlab
- пример:
```
pages: 
    script: 
        - cp -r htmlcov/ public/
    artifacts:
        paths:
        - public
    environment:
        name: test
        url: https://example.gitlab.io/project_name/subdir/
    only:
        - master
```

http://ivrez.gitlab.io/pages_test/master

[![pipeline status](https://gitlab.com/Ivrez/pages_test/badges/master/pipeline.svg)](https://gitlab.com/Ivrez/pages_test/commits/master)
[![coverage report](https://gitlab.com/Ivrez/pages_test/badges/master/coverage.svg)](https://gitlab.com/Ivrez/pages_test/commits/master)
