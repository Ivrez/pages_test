#!/bin/bash
branch_name=`git branch | grep \* | cut -d ' ' -f2`
pytest -p no:warnings --cov-report=term --cov-report=html:"htmlcov/$branch_name" --verbose --cov=src
