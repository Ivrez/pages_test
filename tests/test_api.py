import pytest
from src.api import parse_input


@pytest.fixture
def input_ok():
    return '{"first": "f_value"}'

@pytest.fixture
def input_failed():
    return '{"first": '

def test_parse(input_ok):
    assert not parse_input(input_ok) == False

def test_parse_failed(input_failed):
    assert parse_input(input_failed) == False
